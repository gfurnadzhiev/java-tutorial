package com.mycompany.app;


/**
 *
 * @author gfurnadzhiev
 */
public class Fruit {
	
	// static (per class) field
	private static int totalCount;
	
	// static (per class) method
	/**
     *
     * @return
     */
    public static int getTotalCount() {
		
		return totalCount;
	}
	
	
	/**
     *
     */
    public static void test() {
		
		Fruit apple = new Fruit(120);
		Fruit pear = new Fruit(63);
	        
		System.out.println("Apple weight: " + apple.getWeight());
		System.out.println("Pear weight: " + pear.getWeight());
	        
		// Notice we refer to a static method
		System.out.println("Created " + Fruit.getTotalCount() + " fruits");
	}
	
	// instance field
	private int weight;
	
	/**
     *
     * @param weight
     */
    public Fruit(int weight) {
                
                this.weight = weight;
                
                totalCount++;
        }
	
	// instance method

    /**
     *
     * @return
     */
    public int getWeight() {
                
                return weight;
        }
}