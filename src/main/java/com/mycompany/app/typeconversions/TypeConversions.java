package com.mycompany.app.typeconversions;


/**
 *
 * @author gfurnadzhiev
 */
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.List;
import java.util.Random;


public class TypeConversions {

	public static void stringDemos() {

		//convert string to int and back
		String integerString = "12345";

		int integer = Integer.parseInt(integerString);

		if (integerString.equals(String.valueOf(integer))) {
			System.out.println("Converted string to int and back");
		} else {
			System.out.println("Conversion failed");
		}

		//convert string to boolean and back
		String booleanString = "true";
		boolean bool = Boolean.parseBoolean(booleanString);

		if (booleanString.equals(String.valueOf(bool))) {
			System.out.println("Converted string to boolean and back");
		} else {
			System.out.println("Conversion failed");
		}
		
		booleanString = "false";
		bool = Boolean.parseBoolean(booleanString);

		if (booleanString.equals(String.valueOf(bool))) {
			System.out.println("Converted string to boolean and back");
		} else {
			System.out.println("Conversion failed");
		}

		//convert string to double and back 
		String doubleString = "3.14";
		double doubleVariable = Double.parseDouble(doubleString);

		if (doubleString.equals(String.valueOf(doubleVariable))) {
			System.out.println("Converted string to double and back");
		} else {
			System.out.println("Conversion failed");
		}

		//convert string to byte[] and back 
		String byteArrayString = "abcdef";
		byte[] byteArrayVariable = byteArrayString.getBytes(Charset.forName("UTF-8"));
		String newString = new String(byteArrayVariable, Charset.forName("UTF-8"));
		if (byteArrayString.equals(newString)) {
			System.out.println("Converted string to double and back");
		} else {
			System.out.println("Conversion failed");
		}

	}

	/**
	 *
	 */
	public static void stringArrayDemo() {
		
		String[] stringArray = new String[10];
		
		Random randomNumber = new Random();
		
		for (int i = 0; i < stringArray.length; i++) {
			stringArray[i] = String.valueOf(randomNumber.nextDouble());
		}

		for (String arrayItem : stringArray) {
			System.out.println(arrayItem);
		}

		List<String> stringList = Arrays.asList(stringArray);
		
		for (String listItem : stringList) {
			System.out.println(listItem);
		}
	}

	private TypeConversions() {
	}
}
