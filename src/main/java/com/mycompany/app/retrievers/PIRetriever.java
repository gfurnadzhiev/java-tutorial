package com.mycompany.app.retrievers;


import java.io.IOException;
import java.io.InputStream;
import java.net.URL;


/**
 *
 * @author vd
 */
public class PIRetriever {
	
	
	/**
     *
     * @param url
     * @return
     * @throws IOException
     */
    public static double getPI(final URL url) 
		throws IOException {
		
		InputStream is = url.openStream();
		
		byte[] buf = new byte[100];
		
		int bytesRead = is.read(buf);
		
		System.out.println("Read " + bytesRead + " bytes from URL stream");
		
		String content = new String(buf, 0, bytesRead);
		
		return Double.parseDouble(content);
	}

    private PIRetriever() {
    }
}
