package com.mycompany.app.retrievers;

/**
 *
 * @author vd
 */
public class JARRetrieverThread implements Runnable {
	
	/**
     *
     */
    @Override
	public void run() {
		
		try {
			System.out.println("Started new thread");
			
			JARRetriever.listFiles("http://dzhuvinov.com/tmp/rs256-benchmark.jar");
			
		} catch (Exception e) {
			
			System.err.println("Exception in thread: " + e.getMessage());
			
		} finally {
			
			System.out.println("JAR retriever thread finished");
		}
	}
}
