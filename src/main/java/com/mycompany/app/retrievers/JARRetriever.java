package com.mycompany.app.retrievers;


import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.jar.JarEntry;
import java.util.jar.JarInputStream;


/**
 *
 * @author gf
 */
public class JARRetriever {
	
	
	/**
     *
     * @param url
     * @throws MalformedURLException
     * @throws IOException
     */
    public static void listFiles(String url) 
		throws MalformedURLException,IOException {
		
		URL jarURL = new URL(url);

		JarInputStream jarInputStream = new JarInputStream (jarURL.openStream());
		
		JarEntry jarEntry;
                jarEntry = jarInputStream.getNextJarEntry();
                while(jarEntry!=null){
                    if(!jarEntry.isDirectory()) {
                System.out.println(jarURL + ": " + jarEntry.getName());
            }
                    jarEntry = jarInputStream.getNextJarEntry();
                }
	}

    private JARRetriever() {
    }
}
