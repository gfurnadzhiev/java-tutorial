package com.mycompany.app;
/**
 * Hello world!
 *
 */
import com.mycompany.app.typeconversions.TypeConversions;
public class App {
    
	/**
     *
     * @param args
     * @throws Exception
     */
    public static void main( String[] args )
		throws Exception{
        TypeConversions.stringDemos();
        //TypeConversions.stringArrayDemo();
    }
}