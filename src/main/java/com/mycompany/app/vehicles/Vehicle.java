package com.mycompany.app.vehicles;

import com.mycompany.app.shapes.Taggable;

/**
 * The base class for vehicles.
 */
public class Vehicle implements Taggable {


	/**
     *
     */
    public static void test() {

		Vehicle unknownVehicle = new Vehicle(4);
		unknownVehicle.setColor("red");

		Vehicle car = new Car(4, EngineType.COMBUSTION);

		System.out.println("car seats: " + car.getSeats());
		System.out.println("car color: " + car.getColor());

		// If you're sure car is a motorised vehicle 
		// you can do a casting
		MotorizedVehicle motorizedVehicle = (MotorizedVehicle)car;

		System.out.println("car engine type: " + motorizedVehicle.getEngineType());

		if (car instanceof MotorizedVehicle) {

			MotorizedVehicle mv = (MotorizedVehicle)car;

			System.out.println("car engine type: " + mv.getEngineType());
		}

		try {
			MotorizedVehicle mv = (MotorizedVehicle)unknownVehicle;

		} catch (ClassCastException e) {

			System.out.println("Oops, our vehicle is not motorized!");
		}
	}


	private final int seats;


	private String color;
	
	
	private String tag;


	/**
     *
     * @param seats
     */
    public Vehicle(final int seats) {

		this.seats = seats;
	}


	/**
     *
     * @return
     */
    public int getSeats() {

		return seats;
	}


	/**
     *
     * @param color
     */
    public void setColor(final String color) {

		this.color = color;
	}

	/**
     *
     * @return
     */
    public String getColor() {

		return color;
	}

    @Override
    public void setTag(String tag) {
            
            this.tag = tag;
    }

	@Override
	public String getTag() {
            
            return tag;
    }
}