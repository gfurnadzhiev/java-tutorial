package com.mycompany.app.vehicles;


/**
 *
 * @author gfurnadzhiev
 */
public enum EngineType {


	/**
     *
     */
    COMBUSTION,


	/**
     *
     */
    ELECTRIC,


	/**
     *
     */
    PNEUMATIC
}