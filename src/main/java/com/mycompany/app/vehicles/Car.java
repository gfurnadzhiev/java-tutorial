package com.mycompany.app.vehicles;


/**
 *
 * @author gfurnadzhiev
 */
public class Car extends MotorizedVehicle {


	/**
     *
     * @param seats
     * @param engineType
     */
    public Car(final int seats, final EngineType engineType) {

		super(seats, engineType);
	}
}