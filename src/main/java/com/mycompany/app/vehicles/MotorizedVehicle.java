package com.mycompany.app.vehicles;


/**
 *
 * @author gfurnadzhiev
 */
public class MotorizedVehicle extends Vehicle {


	private final EngineType engineType;


	/**
     *
     * @param seats
     * @param engineType
     */
    public MotorizedVehicle(final int seats, final EngineType engineType) {

		super(seats);

		this.engineType = engineType;
	}


	/**
     *
     * @return
     */
    public EngineType getEngineType() {

		return engineType;
	}
}