package com.mycompany.app.vehicles;


/**
 *
 * @author gfurnadzhiev
 */
public class Motorbike extends MotorizedVehicle {


	/**
     *
     */
    public static final int SEATS = 2;


	/**
     *
     * @param engineType
     */
    public Motorbike(final EngineType engineType) {

		super(SEATS, engineType);
	}
}