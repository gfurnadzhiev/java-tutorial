package com.mycompany.app.furniture;


/**
 * Classes implementing this interface allow to be folded.
 * 
 * For homework: Create a new {@code FoldableChair} class that implements 
 * foldable.
 * 
 * @author vd
 */
public interface Foldable {

    
    	/**
	 * Folds the object.
	 */
	public void fold();
	
	
	/**
	 * Unfolds the object.
	 */
	public void unfold();
	
	
	/**
	 * Checks the folded state.
	 * 
	 * @return {@code true} if the object is folded, else {@code false}.
	 */
	public boolean isFolded();
}
