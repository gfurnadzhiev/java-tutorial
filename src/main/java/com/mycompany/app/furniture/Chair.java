/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.app.furniture;

/**
 *
 * @author vd
 */
public class Chair extends Furniture {
	
	private int legCount;
	
	
	/**
	 * Creates a new metal chair for office use.
	 * 
	 * @param legCount The leg count. Must be at least three.
	 */
	public Chair(int legCount) {
		
		this(Material.METAL, Use.OFFICE, legCount);
	}
	
	/**
	 * Creates a new chair with the specified parameters.
	 * 
	 * @param material
	 * @param use
	 * @param legCount The leg count. Must be at least three.
	 */
	public Chair(Material material, Use use, int legCount) {
		
		super(material,use);
				
		if (legCount < 3) {
                    throw new IllegalArgumentException("The chair must have at least 3 legs");
                }
				
		this.legCount = legCount;
	
	}
	
		
	/**
     *
     * @return
     */
    public int getLegCount(){
		return legCount;
	}
	
}