package com.mycompany.app.furniture;

/**
 *
 * @author vd
 */
public abstract class Furniture{
	
	
	private final Material material;
	
	private final Use use;
	
	
	/**
	 * Creates a new furniture from wood and intended for office use.
	 */
	protected Furniture() {
		
		this(Material.WOOD, Use.OFFICE);
	}
	
	
	/**
	 * Creates a new piece of furniture.
	 * 
	 * @param material The material.
	 * @param use      The use.
	 */
	protected Furniture(final Material material, final Use use) {
		super();
		this.material = material;
		this.use = use;
	}
	
	/**
     *
     * @return
     */
    public Material getMaterial() {
		return material;
	}
	
	/**
     *
     * @return
     */
    public Use getUse() {
		return use;
	} 
    

	// Inner classes, enums, etc...
	/**
     *
     */
    public static enum Material {
		
		/**
             *
             */
            WOOD,
            /**
             *
             */
            METAL,
        /**
         *
         */
        PLASTIC
	}
	
	
	/**
     *
     */
    public static enum Use {
		
		/**
             *
             */
            OFFICE,
            /**
             *
             */
            SHOP,
        /**
         *
         */
        HOME
	}
    
	
}
