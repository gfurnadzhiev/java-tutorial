package com.mycompany.app.furniture;


import java.util.LinkedList;


/**
 * Efficient container for furniture. Allows foldable items to be packed to
 * conserve space.
 * 
 * @author vd
 */
public class EfficientContainer extends LinkedList<Furniture> {
	
	
	
	/**
     *
     * @param furniture
     * @return
     */
    @Override
	public boolean add(Furniture furniture) {
		
		if (furniture instanceof Foldable) {
			
			Foldable foldableItem = (Foldable)furniture;
			
			foldableItem.fold();
		}
		
		return super.add(furniture);
	}
	
        
        /**
         * Removes the furniture item at the specified index.
         * 
         * @see java.util.List#remove
         * 
         * @param index The item index.
         * 
         * @return The removed furniture item. If foldable it will be unfolded.
         */
	@Override
	public Furniture remove(int index) {
		
		Furniture f = super.remove(index);
                
                if (f instanceof Foldable) {
                    
                    Foldable foldableItem = (Foldable)f;
                    if (foldableItem.isFolded()) {
                    
                        foldableItem.unfold();
                        
                    }
                }
                
		return f;
	}
}
