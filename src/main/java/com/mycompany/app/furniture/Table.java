/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.app.furniture;

/**
 *
 * @author vd
 */
public class Table extends Furniture {
	
	
	private static int tableCount;
	
	
	/**
	 * Returns the total count of table objects in the Java Virtual 
	 * Machine.
	 *
	 * @return The total table count.
	 */
	public static int getCount() {
		
		return tableCount;
		
	}
	
	
	private Shape shape;
	private Surface surface;
	private int legCount;
	
	/**
     *
     * @param material
     * @param use
     * @param shape
     * @param surface
     * @param legCount
     */
    public Table(Material material,Use use,Shape shape, Surface surface,int legCount){
		super(material,use);
		
		this.shape = shape;
		
		this.surface = surface;
		
		if (legCount < 1) {
            throw new IllegalArgumentException("The table must have at least 1 leg");
        }
				
		this.legCount = legCount;
		
		tableCount++;
	}
	
	
	/**
     *
     * @return
     */
	public Shape getShape() {
        
                return shape;
        }
	
	/**
     *
     * @return
     */
    public Surface getSurface(){
        
                return surface;
        }
	
	/**
     *
     * @return
     */
    public int getLegCount(){
        
                return legCount;
        }
	

	/**
     *
     */
    public static enum Shape {
		
		/**
         *
         */
        RECTANGLE,
        /**
         *
         */
        OVAL,
        /**
         *
         */
        SQUARE,
        /**
         *
         */
        ROUND
		
	}
	
	/**
     *
     */
    public static enum Surface {
		
		/**
         *
         */
        WOOD,
        /**
         *
         */
        METAL,
        /**
         *
         */
        PLASTIC,
        /**
         *
         */
        GLASS
		
	}
	
}
