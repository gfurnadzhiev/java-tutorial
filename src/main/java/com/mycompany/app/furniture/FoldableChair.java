package com.mycompany.app.furniture;


/**
 *
 * @author gfurnadzhiev
 */
public class FoldableChair extends Chair implements Foldable {

	
	private boolean folded = false;
	

	/**
     *
     * @param legCount
     */
    public FoldableChair(final int legCount) {

		super(legCount);
	}

	/**
     *
     * @param material
     * @param use
     * @param legCount
     */
    public FoldableChair(final Material material, final Use use, final int legCount) {

		super(material, use, legCount);
	}

	@Override
	public void fold() {

		folded = true;

	}

	@Override
	public void unfold() {

		folded = false;

	}

	@Override
	public boolean isFolded() {

		return folded;

	}
}
