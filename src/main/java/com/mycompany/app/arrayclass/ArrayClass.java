package com.mycompany.app.arrayclass;

/**
 *
 * @author gfurnadzhiev
 */
public class ArrayClass{

	/**
     *
     */
    public static int[] someArray = new int[10];

	/**
     *
     */
    public static void fillArray() {

		for(int i=0; i<someArray.length; i++) {
            someArray[i] = i;
        }
	}

	/**
     *
     */
    public static void printArray() {
		for (int i=0;i<someArray.length;i++) {
            System.out.println(someArray[i]);
        }
	}

	/**
     *
     * @return
     */
    public static int sumArray() {
		int sum = 0;
		for (int i=0;i<someArray.length;i++ ) {
            sum +=someArray[i];
        }
		return sum; 

	}

    private ArrayClass() {
    }
}

