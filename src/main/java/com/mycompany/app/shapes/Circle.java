package com.mycompany.app.shapes;

import static com.mycompany.app.shapes.Shape.PI;

/**
 *
 * @author gfurnadzhiev
 */
public class Circle extends Shape {

	private double radius;

	/**
     *
     * @param r
     */
    public Circle(double r) {
		
		super(PI*r*r, 2*PI*r);
		
		if (r <= 0.0) {
                    throw new IllegalArgumentException("The radius must be a positive number");
                }
		
		radius = r;
	}
	
	/**
     *
     * @return
     */
    public double getRadius() {
		
		return radius;
	}
	
	/**
     *
     * @param other
     * @return
     */
    @Override
	public boolean equals(final Object other) {
		
		if (other == null) {
                    return false;
                }
		
		if (! (other instanceof Circle)) {
                    return false;
                }
		
		Circle c = (Circle)other;
		
		return this.radius == c.radius;
	}

    /**
     *
     * @return
     */
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 53 * hash + (int) (Double.doubleToLongBits(this.radius) ^ (Double.doubleToLongBits(this.radius) >>> 32));
        return hash;
    }
}

