package com.mycompany.app.shapes;

/** 
 * Base class in Shapes package. Contains some constants.
 */

public abstract class Shape implements Taggable {

	/**
     *
     */
    protected static final double PI = 3.14159265;
	
	private final double area;
	
	private final double circumference;
	
	private String tag;
	
	
	/**
	 * Creates a new shape.
         * 
         * @param area
         * @param circumference  
         */
	protected Shape(final double area, final double circumference) {
		
		this.area = area;
		this.circumference = circumference;
	}
	
	/**
     *
     * @return
     */
    public double getArea() {
		return area;
	}
	
	
	/**
     *
     * @return
     */
    public double getCircumference() {
		return circumference;
	}
	
	
	@Override
	public void setTag(final String tag) {
		
		this.tag = tag;
	}
	
	
	@Override
	public String getTag() {
		
		return tag;
	}
}

