package com.mycompany.app.shapes;

/**
 *
 * @author gfurnadzhiev
 */
public class Square extends Shape {

	private double side;


	/**
     *
     * @param a
     */
    public Square(double a) {
		
		super(a*a,4*a);
		
		if (a <=0){
			throw new IllegalArgumentException("The side must be a positive number");
		}
		
		side = a;
	}
	
	
	/**
     *
     * @param other
     * @return
     */
    @Override
	public boolean equals(final Object other) {
		
		if (other == null) {
                    return false;
                }
		
		if (! (other instanceof Square)) {
                    return false;
                }
		
		Square c = (Square)other;
		
		return this.side == c.side;
	}
	
	
	/**
     *
     * @return
     */
    public double getSide() {
		
		return side;
		
	}
}

