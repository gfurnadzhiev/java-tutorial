package com.mycompany.app.shapes;

/**
 * Allows objects to be tagged.
 * 
 * @author vd
 */
public interface Taggable {

	/**
	 *  Tags the object.
	 * 
	 * @param tag The tag. May be {@code null} if not specified.
	 */
	public void setTag(final String tag);
	
	/**
	 * Gets the object tag.
	 * 
	 * @return The object tag, {@code null} if not specified.
	 */
	public String getTag();
}
