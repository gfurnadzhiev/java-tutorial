package com.mycompany.app;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;


/**
 * This class demonstrates some of the common Java collections.
 */
public class CollectionsTest {

	
	/**
     *
     */
    public static void arrayTest() {
		
		String[] names = {"Alice", "Bob", "Chris", "Dan"};
		
		String firstName = names[0];
		
		System.out.println("First name: " + firstName);
		
		// iteration with for
		for (int i=0; i < names.length; i++) {
			
			System.out.println("Name at position " + i + ": " + names[i]);
		}
		
		// iteration with for each
		for (String name: names) {
			
			System.out.println("Name: " + name);
		}
		
		
		String[] fruit = new String[10];
		fruit[0] = "apple";
		fruit[1] = "pear";
		fruit[2] = "orange";
	}
	
	
	/**
     *
     */
    public static void listTest() {
		
		List<String> names = new LinkedList<String>();
		
		if (names.isEmpty()) {
			System.out.println("The new list is empty");
		}
		
		names.add("Alice");
		
		System.out.println("Added Alice, new list size is " + names.size());
		
		names.add("Bob");
		names.add("Chris");
		
		String name = names.get(0);
		
		System.out.println("Name at position 0: " + name);
		
		for (int i=0; i < names.size(); i++) {
			
			System.out.println("Name at position " + i + ": " + names.get(i));
		}
		
		
		for (String name2: names){
			System.out.println(name2);
		}
		
		System.out.println("Removed name " + names.remove(1));
		
		for (String name2: names){
			System.out.println(name2);
		}
		
		names.clear();
	}
	
	
	/**
     *
     */
    public static void setTest() {
		
		Set<String> names = new HashSet<String>();
		
		names.add("Alex");
		names.add("Barry");
		names.add("Cindy");
		
		for (String name: names){
			System.out.println("Name in set: " + name);
		}
	}
	
	
	/**
     *
     */
    public static void mapTest() {
		
		// key => value
		Map<String,Integer> ageMap = new HashMap<String,Integer>();
		
		ageMap.put("Alice", 10);
		ageMap.put("Bob", 20);
		ageMap.put("Claire", 30);
		
		System.out.println("The age map contains " + ageMap.size() + " elements");
		
		System.out.println("The age of Alice: " + ageMap.get("Alice"));
		System.out.println("The age of Bob: " + ageMap.get("Bob"));
		System.out.println("The age of John: " + ageMap.get("John"));
		
		// iterate by key set
		for (String name: ageMap.keySet()) {
			
			System.out.println("Got key " + name);
			int age = ageMap.get(name);
			System.out.println("The age of " + name + " is " + age);
		}
		
		// iterate over values
		for (Integer age: ageMap.values()) {
			
			System.out.println("Got age value " + age);
		}
		
		// iterate over entry pair
		for (Map.Entry<String,Integer> mapEntry: ageMap.entrySet()) {
			
			// pair (name,age)
			String n = mapEntry.getKey();
			int age = mapEntry.getValue();
			
			System.out.println("Got entry: " + n + " => " + age);
			
		}
	}
	
	
	/**
     *
     */
    public static void queueTest() {
		
		Queue<String> queue = new LinkedList<String>();
		
		queue.add("job-1");

		System.out.println("Queue size: " + queue.size());

		queue.add("job-2");
		queue.add("job-3");

		for (String job: queue) {

			System.out.println("Queue element: " + job);
		}
	
		System.out.println("Retrieving queue elements:");
		

		String job;

		while ((job = queue.poll()) != null) {

			System.out.println("Next retrieved element: " + job);
		}

		if (queue.isEmpty()) {
            System.out.println("The queue is empty");
        }
	}

    private CollectionsTest() {
    }
}
